var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Items
var Item = new Schema({
  name: {
    type: String
  },
  email: {
    type: String
  },
  location: {
    type: String
  },
  employment_type: {
    type: String
  }
},{
    collection: 'careerItems'
});
module.exports = mongoose.model('CareerItem', Item);
