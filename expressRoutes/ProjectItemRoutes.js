const express = require('express');
const app = express();
const itemRoutes = express.Router();
const FeedParser = require('feedparser');
const request = require('request'); // for fetching the feed

// API
const api = "http://sibedge.com/en/project-rss";

// Defined get data(index or listing) route
itemRoutes.route('/').get(function (req, res) {
    let rssReg = request(api);
    let feedparser = new FeedParser();
    let projects = [];

    rssReg.on('error', function (error) {
        console.log(error)
    });

    rssReg.on('response', function (rssRes) {
        var stream = this; // `this` is `req`, which is a stream

        if (res.statusCode !== 200) {
            this.emit('error', new Error('Bad status code'));
        }
        else {
            stream.pipe(feedparser);
        }
    });

    feedparser.on('error', function (error) {
        console.log(error)
    });

    feedparser.on('end', function () {
        res.json(projects);
    });

    feedparser.on('readable', function () {
        var stream = this; // `this` is `feedparser`, which is a stream
        var meta = this.meta; // **NOTE** the "meta" is always available in the context of the feedparser instance
        var item;

        while (item = stream.read()) {
            let projectItem = {
                title: item.title,
                url: item.link,
                desc: item['rss:description']['#'],
                img: item['rss:content'].img['@'].src
            }

            projects.push(projectItem);
        }
    });
});

module.exports = itemRoutes;
