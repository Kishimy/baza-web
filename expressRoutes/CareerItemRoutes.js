const express = require('express');
const app = express();
const itemRoutes = express.Router();
const https = require('https');

// API
const api = {
	uid: '23.00E',
	url: 'https://www.comeet.co/careers-api/2.0',
	token: '32EFE632E164213141C9E32E13141642CB8'
}

// Defined get data(index or listing) route
itemRoutes.route('/').get(function (req, res) {
	let uri = `${api.url}/company/${api.uid}/positions?token=${api.token}`;

	https.get(uri, (resp) => {
		let data = '';

		resp.on('data', (chunk) => {
			data += chunk;
		});

		resp.on('end', () => {
			res.json(JSON.parse(data));
		});

	}).on("error", (err) => {
		console.log("Error: " + err.message);
	});
});

module.exports = itemRoutes;
