const express = require('express');
const app = express();
const itemRoutes = express.Router();
const https = require('https');

// API
const api = {
	url: 'https://api.instagram.com/v1',
	token: '7392848781.3bbcf57.9f0d0f0328d34ea38bf434723fa91c02'
}

// Defined get data(index or listing) route
itemRoutes.route('/').get(function (req, res) {
	let userId = api.token.split('.')[0];
	let uri = `${api.url}/users/${userId}/media/recent?access_token=${api.token}`;

	https.get(uri, (resp) => {
		let data = '';
	
		resp.on('data', (chunk) => {
			data += chunk;
		});
	
		resp.on('end', () => {
			res.json(JSON.parse(data));
		});
	
	}).on("error", (err) => {
		console.log("Error: " + err.message);
	});
});

module.exports = itemRoutes;