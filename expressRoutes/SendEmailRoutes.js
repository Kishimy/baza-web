const express = require('express');
const app = express();
const itemRoutes = express.Router();
const nodemailer = require('nodemailer');
const multer = require('multer');
const upload = multer({dest:'./uploads/'});

const email = {
    login: "Gaika@baza.one",
    pass: "Skolkovo55"
}

// Defined get data(index or listing) route
itemRoutes.route('/').post(upload.fields([{name: 'file'}, {name: 'email'}, {name: 'text'}]), function (req, res) {
    let nodemailer = require('nodemailer');
    let multer  = require('multer');

    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        auth: {
            user: email.login,
            pass: email.pass
        }
    });

    let mailOptions = {
        from: req.body.email,
        to: 'Gaika@baza.one',
        subject: 'Request from Baza.one',
        html: req.body.text,
        attachments: [
            {
                filename: req.files.file[0].originalname,
                path: req.files.file[0].path
            }
        ]
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
});

module.exports = itemRoutes;