import Vue from 'vue';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import App from './App.vue';
import MainPage from './components/MainPage.vue';
import CareerPage from './components/Careers.vue';
import ProjectsPage from './components/Projects.vue';
import IcoPage from './components/IcoServices.vue';
import ContactsPage from './components/ContactsPage.vue';
import SvgSprite from 'vue-svg-sprite';
import * as VueGoogleMaps from "vue2-google-maps";

var svgUse = require('./assets/js/svgxuse.min.js');

Vue.use(VueAxios, axios);
Vue.use(VueRouter);
Vue.use(SvgSprite, {
	url: '/dist/icons.svg'
});
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyAUyFMIEekrmpLXu06p3lEWOX4f__OcWF4",
    libraries: "places"
  }
});

const routes = [
	{
		name: 'MainPage',
		path: '/',
		component: MainPage
	},
	{
		name: 'Career',
		path: '/career',
		component: CareerPage
	},
	{
		name: 'Projects',
		path: '/projects',
		component: ProjectsPage
	},
	{
		name: 'ICO',
		path: '/ico-services',
		component: IcoPage
	},
	{
		name: 'Contacts',
		path: '/contacts',
		component: ContactsPage
	}
]

const router = new VueRouter({ mode: 'history', routes: routes });
Vue.prototype.$api = 'http://localhost:4000';
new Vue(Vue.util.extend({ router }, App)).$mount('#app');
