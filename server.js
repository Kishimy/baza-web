const express = require('express'),
		path = require('path'),
		bodyParser = require('body-parser'),
		cors = require('cors'),
		CareerItemRoutes = require('./expressRoutes/CareerItemRoutes');
		InstagramItemRoutes = require('./expressRoutes/InstagramItemRoutes');
		ProjectItemRoutes = require('./expressRoutes/ProjectItemRoutes');
		SendEmailRoutes = require('./expressRoutes/SendEmailRoutes');

const app = express();
const server = require('http').createServer(app);
app.use(express.static('public'));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(cors());
app.use('/careerItems', CareerItemRoutes);
app.use('/instagramItems', InstagramItemRoutes);
app.use('/projectItems', ProjectItemRoutes);
app.use('/sendEmail', SendEmailRoutes);

const port = process.env.PORT || 4000;

server.listen(port, function (error) {
	if (error) {
			console.error(error)
	} else {
			console.info("==> 🌎  Listening on port %s.", port)
	}
})